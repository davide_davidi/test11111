/**
 * Created by dzeri on 25/09/2014.
 */

"use strict";

var Test = function () {
    this.write = function (text) {
        console.log(text);
    };
};

module.exports = Test;
